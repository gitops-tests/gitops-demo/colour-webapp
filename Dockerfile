FROM python:3.9-alpine

EXPOSE 8080

WORKDIR /app

COPY /src/webapp.py /src/required_packages.txt /app

RUN pip install -r required_packages.txt

ENV APP_VERSION=v2

ENTRYPOINT ["python", "app.py"]