from flask import Flask
from flask import render_template
import socket
import random
import os
import argparse

app = Flask(__name__)

# Get color from environment variable
my_version = os.environ.get('APP_VERSION')

# Colors defined based on the version envrionment variable:
color_codes = {
    "v1": "#2980b9", # blue
    "v2": "#16a085", # green
    "v3": "#e74c3c"  # red
}

@app.route("/")
def main():
    return render_template('hello.html', name=socket.gethostname(), color=color_codes[my_version], version=my_version)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)